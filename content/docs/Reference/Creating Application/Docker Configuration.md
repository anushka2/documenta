---
title: Docker Configuration
weight: 2
bookToc: true
---

# Docker build Configuration 

Docker Build Configuration is required to execute CI Pipeline for your application.

<br>

![Docker Configuration](../../docker_config.jpg "Docker Build Configurations")



Key  | Description
-----|-----
`Repository` | Select the path of Repository
`Docker File Path` | Give a Relative Path for your Dockerfile
`Docker registry` | Select the Docker Registry to be used
`Docker repository` | Name of the Docker Repository
`Key` | Key parameter for your Docker Build
`Value` | Value for a given key for your Docker build 

<br>


<br>



Click on **Save Configuration** to save the Docker Build Configuration.
