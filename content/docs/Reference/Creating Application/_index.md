---
bookCollapseSection: true
weight: 20
---



Welcome! This is the documentation for Creating Applications

**Parts of Documentation** 

[Git Material](https://devtron.gitlab.io/tutorials-dev/docs/reference/creating-application/git-material/)



[Docker Configuration](https://devtron.gitlab.io/tutorials-dev/docs/reference/creating-application/docker-configuration/) 



[Deployment Template](https://devtron.gitlab.io/tutorials-dev/docs/reference/creating-application/deployment-template/) 



[Workflows](https://devtron.gitlab.io/tutorials-dev/docs/reference/creating-application/workflows/) 



[Config Maps](https://devtron.gitlab.io/tutorials-dev/docs/reference/creating-application/config-maps/) 



[Secrets](https://devtron.gitlab.io/tutorials-dev/docs/reference/creating-application/secrets/) 



[Environment Overrides](https://devtron.gitlab.io/tutorials-dev/docs/reference/creating-application/environment-overrides/) 
