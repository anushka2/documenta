---
bookCollapseSection: false
weight: 21
---

# Cloning Apps

![Cloning Apps](./d1.JPG "Duplicate Templates")

Select Add New App to add a new app


![Cloning Apps](./d2.JPG "Duplicate Templates")

Key | Description
---- | -----
`App Name` | Name of the new app
`Project` | Project name
`Template` | App from which the template has to be copied

![Cloning Apps](./d4.JPG "Duplicate Templates")

Select Create App to create App

![Cloning Apps](./d5.JPG "Duplicate Templates")

New app with duplicate template is created

