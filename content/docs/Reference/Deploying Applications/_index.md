---
bookCollapseSection: true
weight: 23
---


Welcome! This is the documentation for Deploying Applications

**Parts of Documentation** 

[Triggering CI](https://devtron.gitlab.io/tutorials-dev/docs/reference/deploying-applications/triggering-ci/)
<br>

[Triggering CD](https://devtron.gitlab.io/tutorials-dev/docs/reference/deploying-applications/triggering-cd/) 



